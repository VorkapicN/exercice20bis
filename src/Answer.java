import java.util.ArrayList;

public class Answer implements Post{
    private Member auteur;
    private String contenu;
    private Question parent;
    private ArrayList<Member> voters = new ArrayList<>();

    public Answer(Member auteur, Question parent, String contenu) {
        this.auteur = auteur;
        this.contenu = contenu;
        this.parent = parent;
    }

    public void addVote(Member m) {
        int cpt = 0;
        for (Answer a : parent.getReponsesList()) {
            for (Member voter : a.voters) {
                if (m == voter)
                    ++cpt;
            }
        }
        if (cpt < 2)
            voters.add(m);
    }

    @Override
    public String toString() {
        return "- Réponse de " + this.auteur.getNom() + " : " + this.parent.getTitre() + " " + this.contenu +
                " " + voters.size();
    }

    @Override
    public String getBody() {
        return contenu;
    }

}
