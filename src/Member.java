import java.util.ArrayList;
import java.util.List;

public class Member {
    private String nom;
    private List<Post> list = new ArrayList<>();

    public Member(String nom) {
        this.nom = nom;
    }

    public Question askQuestion(String titre, String contenu) {
        Question q = new Question(titre, contenu, this);
        this.list.add(q);
        return q;
    }

    public Answer answer(Question q, String s) {
        Answer a = new Answer(this, q, s);
        q.addAnswer(a);
        this.list.add(a);
        return a;
    }

    public void vote(Question q) {
        q.addVote();
    }

    public void vote(Answer a) {
        a.addVote(this);
    }

    public String getNom() {
        return nom;
    }

    public List<Post> getPosts() {
        return this.list;
    }

    public String toString() {
        return this.nom;
    }

}
