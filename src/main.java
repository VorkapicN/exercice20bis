public class main {

    public static void main(String[] args) {

        Member alain = new Member (" Alain ");
        Member boris = new Member (" Boris ");
        Question q1 = boris . askQuestion ("Q1", " Question 1");
        Question q2 = boris . askQuestion ("Q2", " Question 2");
        Answer a1 = alain . answer (q1 , "Ré ponse 1");
        Answer a2 = boris . answer (q1 , "Ré ponse 2");
        Answer a3 = boris . answer (q1 , "Ré ponse 3");
        Answer a4 = boris . answer (q2 , "Ré ponse 4");
        alain . vote ( a1 );
        alain . vote ( a2 );
        alain . vote ( a2 );
        alain . vote ( a3 );
        System . out . println ( q1 );
        alain . vote ( a4 );
        alain . vote ( a4 );
        System . out . println ( q2 );
    }
}
