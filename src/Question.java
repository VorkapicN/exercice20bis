import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Question implements Post{
    private Member auteur;
    private String titre;
    private String contenu;
    private int nbDeVotes;

    public List<Answer> getReponsesList() {
        return reponsesList;
    }

    private List<Answer> reponsesList = new ArrayList<>();

    public Question(String titre, String contenu, Member auteur) {
        this.titre = titre;
        this.contenu = contenu;
        this.auteur = auteur;
    }

    public Member getAuteur() {
        return auteur;
    }

    public String getTitre() {
        return titre;
    }

    public String getContenu() {
        return contenu;
    }

    public int getNbDeVotes() {
        return nbDeVotes;
    }

    public void addAnswer(Answer answer) {
        reponsesList.add(answer);
    }

    public void addVote() {
        ++nbDeVotes;
    }

    @Override
    public String toString() {
        return "Question de " + this.auteur.getNom() + " : " + getTitre() + " " + "\"" + getContenu() + "\"" + nbDeVotes + reponsesList;
    }

    @Override
    public String getBody() {
        return contenu;
    }
}
